#!/bin/bash
##
## This is the SLURM batch job to compile WRF model version 4.1.2
## on galileo cluster         
##
## by Dario B. Giaiotti
## Department of Physics
## University of Trieste 
## e-mail dgiaiotti@units.it
##
## July 28, 2019
##
## System Galileo
## Model: IBM NeXtScale
## Architecture: Linux Infiniband Cluster 
## Nodes: 360 Processors: 2 x 18-cores Intel Xeon E5-2697 v4 (Broadwell) at 2.30 GHz
## Cores: 36 cores/node, 12.960 cores in total
## RAM: 128 GB/node, 3 GB/core
## Internal Network: Infiniband with 4x QDR switches
## Disk Space:2,500 TB of local storage
## Peak Performance: 0.5 PFlop/s 
##
## and 
##
## Nodes: 15 
## Processors: 2 x 8-cores Intel Haswell 2.40 Ghz + 2 nVidia K80 GPUs
## Cores: 16 cores/node, 576 cores in total
## RAM: 128 GB/node
## Internal Network: Infiniband with 4x QDR switches
##
## SLURM 17.11                                                                 *
##
##
##
##
##           SLURM DIRECTIVES HERE BELOW
##
## -----------------------------------------------------------------------------
## Remind how to request nodes and use them to run MPI and OpenMP codes
#
#  --nodes=NN                  number of allocate nodes (short version -N)
#  --ntasks=nn                 maximum of number tasks within the allocation (short version -n)
#  --ntasks-per-node=CC        number of tasks/processes per node
#  --cpus-per-task=TT          number of threads/cpus per task i.e cpus per node (short version -c)
#
# For example for a MPI or MPI/OpenMP mixed job (2 MPI processes and 8 threads):
#  #SBATCH --nodes=2
#  #SBATCH --ntasks-per-node=8
#
# For a serial job for example:
#  #SBATCH --nodes=1
#  #SBATCH --ntasks-per-node=1
#  #SBATCH --cpus-per-task=1
#
# Other useful directives 
#
##SBATCH --account=<account_no>          --> name of the project to be accounted to ("saldo -b" for a list of projects)
##SBATCH --job-name=<name>               --> job name
##SBATCH --partition=<destination>       --> partition/queue destination. For a list and description of available partitions, 
##                                           please refer to the specific cluster description of the guide.
##SBATCH --qos=<qos_name>                --> quality of service. Please refer to the specific cluster description of the guide.
##SBATCH --mail-type=<mail_events>       --> specify email notification (NONE, BEGIN, END, FAIL, REQUEUE, ALL)
##SBATCH --mail-user=<user_list>         --> set email destination (email address)
##
##SBATCH --chdir=<directory>             --> Set  the working directory of the batch script to directory before it is executed. 
##                                           The path can be specified as full path or relative path to the directory where the
##                                           command is executed.
##
##SBATCH --output=<out_file>             --> redirects output file (default, if missing, is slurm-<Pid> containing merged output and error file)
##SBATCH --error=<err_file>              --> redirects error file (as above)
##
##       Filename pattern sbatch allows for a filename pattern to contain one or more replacement symbols, 
##       which are a percent sign "%" followed by a letter (e.g. %j).
##
##            \\ Do not process any of the replacement symbols.
##            %% The character "%".
##            %A Job array's master job allocation number.
##            %a Job array ID (index) number.
##            %J jobid.stepid of the running job. (e.g. "128.0")
##            %j jobid of the running job.
##            %N short hostname. This will create a separate IO file per node.
##            %n Node identifier relative to current job (e.g. "0" is the first node of the running job) 
##               This will create a separate IO file per node.
##            %s stepid of the running job.
##            %t task identifier (rank) relative to current job. This will create a separate IO file per task.
##            %u User name.
##            %x Job name.
##
##
## -----------------------------------------------------------------------------
##
## The default memory depends on the partition/queue you are working with.
## You can specify the requested memory with the --mem=<value> directive up
## to maximum memory available on the nodes.
## The default measurement unit for memory requests is the Megabyte
###SBATCH --mem=10000 # i.e 10 GB
##
## -----------------------------------------------------------------------------
##   When the job is submitted it will inherit all environment variables that
##   were set in the parent shell on the login node, not just those set by the
##   module command . Care should be taken to make sure that there are no other
##   environment variables in the parent shell that could cause problems in the
##   batch script.
##SBATCH --export=ALL
##
## -----------------------------------------------------------------------------
##   This option will tell sbatch to retrieve the login environment variables
##   for the user specified in the --uid option. Be aware that any environment
##   variables already set in sbatch's environment will take precedence over
##   any environment variables in the user's login environment.
###SBATCH --get-user-env[=timeout][mode]
##
## -----------------------------------------------------------------------------
##   Notify user by email when certain event types occur.  Valid type values are
##   NONE, BEGIN, END, FAIL, REQUEUE, ALL (equivalent to BEGIN, END, FAIL, REQUEUE,
##   and STAGE_OUT), STAGE_OUT (burst buffer stage out and teardown completed),
##   TIME_LIMIT, TIME_LIMIT_90 (reached 90% of time limit), TIME_LIMIT_80 (80%),
##   TIME_LIMIT_50 (50%) and ARRAY_TASKS (send emails for each array task).
##   Multiple type values may be specified in a comma separated list.
###SBATCH --mail-type=TIME_LIMIT_90
##
## -----------------------------------------------------------------------------
## =============================
## |  DIRECTIVES FOR THIS JOB  |
## =============================
#SBATCH --partition=gll_usr_prod
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=1
#SBATCH --time=3:00:00 
#SBATCH --account=uTS19_Giaiotti_0
#SBATCH --job-name=compile_WRF
#SBATCH --output=%x-%j.out
#SBATCH --error=%x-%j.err
#SBATCH --mail-type=TIME_LIMIT_90
#SBATCH --mail-user=dgiaiotti@units.it
#SBATCH --chdir=/gpfs/scratch/userexternal/dgiaiott    
#
#           SLURM DIRECTIVES HERE ABOVE
#
# -----------------------------------------------------------------------------
#
#
# ===================================
# |  S T A R T   O F   S C R I P T  |
# ===================================
#
# -----------------------------------------------------------------------------
#
#
#   GENERAL SETTINGS FOR THE WHOLE SCRIPT
#
    set -e            # Exit if any command ends with non zero exit status
    set -u            # Error if any variable is unset
#    set -x            # Verbose each command
    EXIT_STATUS=0     # Default exit status of this job
    D0=$(date -u +%s) # Get the time (seconds) at the beginning
    echo -e "\n\tScript run START: $(date -u +%F' '%T)"
#
#   CONSTANTS AND PARAMETERS               
#
    MODULE_TO_LOAD="profile/base autoload intel intelmpi"
    COMPILE_DIR="${WORK}/wrf_model/wrf-model/WRF"
    NETCDF_DIR="${WORK}/wrf_model/wrf-model/lib/netcdf-4.4.1.1"
    DIR2CHECK="./run"
    WHAT_COMPILE="em_real"
#
#
#   LOAD NEEDED ENVIRONMENTAL MODULES
#   From initialization file
    module load $MODULE_TO_LOAD
    module list
#
#
#
#   SOME DIAGNOSTIC USEFUL FOR DEBUG
#
#
cat <<EOF
===================================================================================
ENVIRONMENT VARIABLES
       All the environmental variables available from env command
EOF
   env
cat <<EOF
===================================================================================
INPUT ENVIRONMENT VARIABLES
       Upon  startup,  sbatch  will  read and handle the options set in the following
       environment variables.
EOF
   env | grep "SBATCH" || echo -e "No SBATCH environmental variables found!\n"
cat <<EOF
===================================================================================
OUTPUT ENVIRONMENT VARIABLES
       The Slurm controller will set the following variables in the environment of
       the batch script.
EOF
   env | grep "MPIRUN_"  || echo -e "No MPIRUN_ environmental variables found!\n"
   env | grep "SLURM"    || echo -e "No SLURM  environmental variables found!\n"
cat <<EOF
===================================================================================
EOF
#
#
#
#   HERE WE GO WITH THE WORK            
#
#
#
#   Move to compilation directory
#
    cd $COMPILE_DIR
    echo -e "\n\tI am in directory: $(pwd)"
#
#   Export the needed environmental variables
#
    echo -e "\n\tI am exporting NETCDF=${NETCDF_DIR}"
    export NETCDF=${NETCDF_DIR}
#
#   Compiling the code     
#
    OKKO=0
    COMPILE_LOG_FILE="wrf_compile_$(date -u +%Y%m%d-%H%M%S).log" 
    echo -e "\n\tI am going to start the compilation.\n\tLog file is: ${COMPILE_LOG_FILE}"
    ./compile  $WHAT_COMPILE > ${COMPILE_LOG_FILE} 2>&1 || OKKO=1
    if [[ $OKKO -eq 0 ]];then
       echo -e "\n\tWRF model compilation successfully completed.\n\tIn ${DIR2CHECK} executable files are:"
       find $DIR2CHECK -executable | sed s/^/\\t/g
    else
       echo -e "\n\tWRF model compilation completed with errors.. Please check!!!\n"
    fi
    echo -e "\n\tAll done\n"
#
#   SOME INFORMATION BEFORE TO EXIT
#
    D1=$(date -u +%s) # Get the time (seconds) at the end
    echo -e "\n\tScripts run END: $(date -u +%F' '%T)"
    echo -e "\tTotal $(($D1 - $D0)) seconds  [$((($D1 - $D0)/60)) minutes]"
    echo -e "\n\n\tTo get the summary of resources used in this job:"
    echo -e "\t sacct -p -l -j $SLURM_JOBID \n\n"
    exit $EXIT_STATUS

#
# ===============================
# |  E N D   O F   S C R I P T  |
# ===============================
#
