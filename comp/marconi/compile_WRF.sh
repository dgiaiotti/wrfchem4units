#!/bin/bash
cd /marconi_work/uTS17_Dida_0/wrf_model/wrf-intel/wrf-3.9.1.1/WRFV3
module load intel/pe-xe-2017--binary intelmpi/2017--binary netcdf/4.4.1--intel--pe-xe-2017--binary netcdff/4.4.4--intel--pe-xe-2017--binary

export NETCDF=$NETCDFF_HOME


/marconi_work/uTS17_Dida_0/wrf_model/wrf-intel/wrf-3.9.1.1/WRFV3/compile wrf > /marconi_work/uTS17_Dida_0/wrf_model/wrf-intel/wrf-3.9.1.1/WRFV3/compile.log 2>&1

exit 0
