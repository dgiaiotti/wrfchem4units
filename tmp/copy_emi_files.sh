#!/bin/bash
#
# Copy the emissions for WRF-Chem run as expected
# by  convert_emiss.exe
#
# Input are found listing files with known extension
#  RUNDIR="/marconi_scratch/userexternal/dgiaiott/EYJAFJOLL_V3.6/wrf_run/"
#  RUNDIR="/marconi_scratch/userexternal/dgiaiott/EYJAFJOLL/wrf_run/"
  RUNDIR="/marconi_scratch/userexternal/dgiaiott/EYJAFJOLL_V3.9/wrf_run"
#
# files *-g1-ab.bin
  for F in $(ls -1 *-g1-ab.bin);do
      cp -v $F $RUNDIR/emissopt3_d01
  done
# files *-g1-bb.bin
  for F in $(ls -1 *-g1-bb.bin);do
      cp -v $F $RUNDIR/emissfire_d01
  done
# files *-g1-volc.bin
  for F in $(ls -1 *-g1-volc.bin);do
      cp -v $F $RUNDIR/volc_d01     
  done
# files *-g1-gocartBG.bin
  for F in $(ls -1 *-g1-gocartBG.bin);do
      cp -v $F $RUNDIR/wrf_gocart_backg
  done
exit 0
