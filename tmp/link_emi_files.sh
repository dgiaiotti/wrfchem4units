#!/bin/bash
#
# Link the emissions for WRF-Chem run as expected
# by  convert_emiss.exe
#
# Input are gound listing files with known extension
#
# files *-g1-ab.bin
  for F in $(ls -1 *-g1-ab.bin);do
      ln -svf $F emissopt3_d01
  done
# files *-g1-bb.bin
  for F in $(ls -1 *-g1-bb.bin);do
      ln -svf $F emissfire_d01 
  done
# files *-g1-volc.bin
  for F in $(ls -1 *-g1-volc.bin);do
      ln -svf $F volc_d01             
  done
# files *-g1-gocartBG.bin
  for F in $(ls -1 *-g1-gocartBG.bin);do
      ln -svf $F wrf_gocart_backg
  done
exit 0
