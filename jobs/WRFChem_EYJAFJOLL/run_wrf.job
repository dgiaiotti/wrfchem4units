#!/bin/bash
#
# This is the SLURM batch job to run the wrf.exe part of
# WRF model in the real case 
#
# sytem A1 (Broadwell)
# 
# Model: Lenovo NeXtScale
# Racks: 21 
# Nodes: 1.512
# Processors: 2 x 18-cores Intel Xeon E5-2697 v4 (Broadwell) at 2.30 GHz
# Cores: 36 cores/node, 54.432 cores in total
# RAM: 128 GB/node, 3.5 GB/core
#
#
#           SLURM DIRECTIVES HERE BELOW
#
# -----------------------------------------------------------------------------
#SBATCH -N10 -n36                  # 12 tasks on 1 node
#SBATCH --error=%u-%x-%j.err         # std-error file
#SBATCH --output=%u-%x-%j.out         # std-output file
#SBATCH --account=uTS18_Giaiotti  # account number
#
# -----------------------------------------------------------------------------
# For long lasting compilation use partition=bdw_usr_prod
# because it allows more than 30 minutes
#
#SBATCH --time=12:30:00           # time limits: 24.0 hours
#SBATCH --partition=bdw_usr_prod   # partition and queue to be used
#
# -----------------------------------------------------------------------------
# For short lasting compilation use partition=bdw_usr_dbg
# because it allows up to 30 minutes
#
##SBATCH --time=25:00              # time limits: 0.5 hours
##SBATCH --partition=bdw_usr_dbg   # partition to be used
#
# -----------------------------------------------------------------------------
# For serial exexutions use partition=bdw_all_serial
# because it allows up to 4 hours
# It runs on the login nodes and it is designed for pre/post-processing serial
# analysis, and for moving your data (via rsync, scp etc.) in case more than
# 10 minutes are required to complete the data transfer.
#
##SBATCH --time=01:30:00              # time limits: 4.0 hours
##SBATCH --partition=bdw_all_serial   # partition to be used
#
# -----------------------------------------------------------------------------
#   When the job is submitted it will inherit all environment variables that 
#   were set in the parent shell on the login node, not just those set by the 
#   module command . Care should be taken to make sure that there are no other 
#   environment variables in the parent shell that could cause problems in the 
#   batch script.
#SBATCH --export=ALL
#
# -----------------------------------------------------------------------------
#   This option will tell sbatch to retrieve the login environment variables 
#   for the user specified in the --uid option. Be aware that any environment 
#   variables already set in sbatch's environment will take precedence over 
#   any environment variables in the user's login environment. 
##SBATCH --get-user-env[=timeout][mode]
#
# -----------------------------------------------------------------------------
#   Notify user by email when certain event types occur.  Valid type values are 
#   NONE, BEGIN, END, FAIL, REQUEUE, ALL (equivalent to BEGIN, END, FAIL, REQUEUE, 
#   and STAGE_OUT), STAGE_OUT (burst buffer stage out and teardown completed), 
#   TIME_LIMIT, TIME_LIMIT_90 (reached 90% of time limit), TIME_LIMIT_80 (80%), 
#   TIME_LIMIT_50 (50%) and ARRAY_TASKS (send emails for each array task). 
#   Multiple type values may be specified in a comma separated list. 
#SBATCH --mail-type=TIME_LIMIT_90
#
# -----------------------------------------------------------------------------
#   User to receive email notification of state changes as defined by --mail-type. 
#SBATCH --mail-user=dgiaiotti@units.it
#
# -----------------------------------------------------------------------------
#   Directory where the job starts
#SBATCH -D /marconi_scratch/userexternal/dgiaiott
#
#
#           SLURM DIRECTIVES HERE ABOVE
#
# -----------------------------------------------------------------------------
#
#
#   GENERAL SETTINGS FOR THE WHOLE SCRIPT
#
    set -e            # Exit if any command ends with non zero exit status
    set -u            # Error if any variable is unset
    set -x            # Verbose each command
    EXIT_STATUS=0     # Default exit status of this job
    D0=$(date -u +%s) # Get the time (seconds) at the beginning
    echo -e "\n\tWRF START: $(date -u +%F' '%T)"
    # Initialization file reques to run the simulazion
    INI_FILE="/marconi/home/userexternal/dgiaiott/src/wrfchem4units/etc/wrf_chem-simulation.ini.EYJAFJOLL"
#
#   CHECK INITIALIZATION FILE AVAILABILITY THEN LOAD IT	
#
    echo -en "\n\n\tLoking for initialization file for this simulation ..."
    if [[ -e $INI_FILE ]]; then
       echo -en "\tFound.\n\tIt is: $INI_FILE \n\tLoad initialization file ..."
       GOON=0
       source $INI_FILE || GOON=1
       if [[ $GOON -eq 0 ]];then 
          echo -en " successfuly loaded."
       else
          echo -en " ERROR in loading initialization file. I cannot continue.\n\tEXIT\n\n"
          EXIT_STATUS=2; exit $EXIT_STATUS
       fi
    else
       echo -en "... NOT FOUND!!!\n\tIt should be: $INI_FILE\n\tI cannot continue.\n\tEXIT\n\n" 
       EXIT_STATUS=1; exit $EXIT_STATUS
    fi
#
#
#   LOAD NEEDED ENVIRONMENTAL MODULES
#   From initialization file 
  module load $MODULE_TO_LOAD        
  module list
#
#
#
#   SOME DIAGNOSTIC USEFUL FOR DEBUG 
#
#
cat <<EOF
===================================================================================
ENVIRONMENT VARIABLES
       All the environmental variables available from env command
EOF
   env
cat <<EOF
===================================================================================
INPUT ENVIRONMENT VARIABLES
       Upon  startup,  sbatch  will  read and handle the options set in the following 
       environment variables.
EOF
   env | grep "SBATCH" || echo -e "No SBATCH environmental variables found!\n"
cat <<EOF
===================================================================================
OUTPUT ENVIRONMENT VARIABLES
       The Slurm controller will set the following variables in the environment of 
       the batch script.
EOF
   env | grep "MPIRUN_"  || echo -e "No MPIRUN_ environmental variables found!\n"
   env | grep "SLURM"    || echo -e "No SLURM  environmental variables found!\n"
cat <<EOF
===================================================================================
EOF
#   SET GENERAL PARAMETERS AND VARIABLES
#   From initialization file 
#
#
#   EXPORT VARIABLES REQUIRED IN THE ENVIRONMENT
#   From initialization file 
#
  echo -e "\n\tSome parameters are:\n\tHOME_WORK=$HOME_WORK\n\tWRFMODEL_ROOT=$WRFMODEL_ROOT" 
  echo -e "\tPATH=${PATH}"
  echo -e "\tLIBPATH=${LIBPATH}"
  echo -e "\tLD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
#
# Create the directory where the wrf.exe will run and the 
# Boundary conditions are going to be moved   
#
  if [[ ! -d ${WRF_RUN_DIR} ]];then mkdir -p ${WRF_RUN_DIR} ; fi
  cd ${WRF_RUN_DIR}
  echo -e "\n\tNow I am in: $(pwd)\n"
#
# Link all executables and the files required ti run the wrf.exe 
#
  for F in $(ls ${WRF_DIR}/*);do ln -svf $F | sed s/^/\\t/g ; done
  if [[ -e namelist.input ]];then rm namelist.input ;fi
#
# Generate the namelist file to run WRF (wrf.exe in this case)
#
# First compute run length in hours
  RUNHH=$((($(date -u -d "${DE:0:8} ${DE:8:2}" +%s) - $(date -u -d "${DS:0:8} ${DS:8:2}" +%s))/3600))
  echo -e "\n\tCustomize namelist for WRF. START DATE is: $DS - END DATE is: $DE (Run hours: $RUNHH).\n\n"
  SEDFILE="./$(mktemp -u sedfile.XXXXX)"
  cat << EOF > $SEDFILE
    s/%SYYYY%/${DS:0:4}/g
    s/%SMM%/${DS:4:2}/g
    s/%SDD%/${DS:6:2}/g
    s/%SHH%/${DS:8:2}/g
    s/%EYYYY%/${DE:0:4}/g
    s/%EMM%/${DE:4:2}/g
    s/%EDD%/${DE:6:2}/g
    s/%EHH%/${DE:8:2}/g
    s/%RUNHH%/${RUNHH}/g
EOF
  sed -f $SEDFILE $WRF_NAMELIST_TPL > namelist.input
  if [[ -e $SEDFILE ]];then rm $SEDFILE ;fi 
#
# Run wrf.exe
#
  echo -e "\n\tRunning wrf.exe\n"
  srun --mpi=pmi2   ./wrf.exe
  echo -e "\n\tCompleted\n"
#
# Some diagnostics before to leave on putputs
#
  ls -l $WRF_RUN_DIR | sed s/^/\\t/g
#
#   SOME INFORMATION BEFORE TO EXIT 
#
    D1=$(date -u +%s) # Get the time (seconds) at the end             
    echo -e "\n\tWRF run END: $(date -u +%F' '%T)"
    echo -e "\tTotal $(($D1 - $D0)) seconds  [$((($D1 - $D0)/60)) minutes]"
    echo -e "\n\n\tTo get the summary of resources used in this job:"
    echo -e "\t sacct -p -l -j $SLURM_JOBID \n\n"
    exit $EXIT_STATUS
