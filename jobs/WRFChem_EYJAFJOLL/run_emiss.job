#!/bin/bash
#
# This is the SLURM batch job to run the  prep_chem_sources_RADM_WRF_FIM_.exe, 
# the convert_emiss.exe and the real.exe  generating the pollutant sources   
#
# WRF model in the real case 
#
# sytem A1 (Broadwell)
# 
# Model: Lenovo NeXtScale
# Racks: 21 
# Nodes: 1.512
# Processors: 2 x 18-cores Intel Xeon E5-2697 v4 (Broadwell) at 2.30 GHz
# Cores: 36 cores/node, 54.432 cores in total
# RAM: 128 GB/node, 3.5 GB/core
#
#
#           SLURM DIRECTIVES HERE BELOW
#
# -----------------------------------------------------------------------------
#SBATCH -N1 -n36                  # 12 tasks on 1 node
#SBATCH --error=%u-%x-%j.err         # std-error file
#SBATCH --output=%u-%x-%j.out         # std-output file
#SBATCH --account=uTS18_Giaiotti  # account number
#
# -----------------------------------------------------------------------------
# For long lasting compilation use partition=bdw_usr_prod
# because it allows more than 30 minutes
#
##SBATCH --time=03:30:00           # time limits: 24.0 hours
##SBATCH --partition=bdw_usr_prod   # partition and queue to be used
#
# -----------------------------------------------------------------------------
# For short lasting compilation use partition=bdw_usr_dbg
# because it allows up to 30 minutes
#
#SBATCH --time=20:00              # time limits: 0.5 hours
#SBATCH --partition=bdw_usr_dbg   # partition to be used
#
# -----------------------------------------------------------------------------
# For serial exexutions use partition=bdw_all_serial
# because it allows up to 4 hours
# It runs on the login nodes and it is designed for pre/post-processing serial
# analysis, and for moving your data (via rsync, scp etc.) in case more than
# 10 minutes are required to complete the data transfer.
#
##SBATCH --time=01:30:00              # time limits: 4.0 hours
##SBATCH --partition=bdw_all_serial   # partition to be used
#
# -----------------------------------------------------------------------------
#   When the job is submitted it will inherit all environment variables that 
#   were set in the parent shell on the login node, not just those set by the 
#   module command . Care should be taken to make sure that there are no other 
#   environment variables in the parent shell that could cause problems in the 
#   batch script.
#SBATCH --export=ALL
#
# -----------------------------------------------------------------------------
#   This option will tell sbatch to retrieve the login environment variables 
#   for the user specified in the --uid option. Be aware that any environment 
#   variables already set in sbatch's environment will take precedence over 
#   any environment variables in the user's login environment. 
##SBATCH --get-user-env[=timeout][mode]
#
# -----------------------------------------------------------------------------
#   Notify user by email when certain event types occur.  Valid type values are 
#   NONE, BEGIN, END, FAIL, REQUEUE, ALL (equivalent to BEGIN, END, FAIL, REQUEUE, 
#   and STAGE_OUT), STAGE_OUT (burst buffer stage out and teardown completed), 
#   TIME_LIMIT, TIME_LIMIT_90 (reached 90% of time limit), TIME_LIMIT_80 (80%), 
#   TIME_LIMIT_50 (50%) and ARRAY_TASKS (send emails for each array task). 
#   Multiple type values may be specified in a comma separated list. 
#SBATCH --mail-type=TIME_LIMIT_90
#
# -----------------------------------------------------------------------------
#   User to receive email notification of state changes as defined by --mail-type. 
#SBATCH --mail-user=dgiaiotti@units.it
#
# -----------------------------------------------------------------------------
#   Directory where the job starts
#SBATCH -D /marconi_scratch/userexternal/dgiaiott
#
#
#           SLURM DIRECTIVES HERE ABOVE
#
# -----------------------------------------------------------------------------
#
#
#   GENERAL SETTINGS FOR THE WHOLE SCRIPT
#
    set -e            # Exit if any command ends with non zero exit status
    set -u            # Error if any variable is unset
    set -x            # Verbose each command
    EXIT_STATUS=0     # Default exit status of this job
    D0=$(date -u +%s) # Get the time (seconds) at the beginning
    echo -e "\n\tWPS real run START: $(date -u +%F' '%T)"
    # Initialization file reques to run the simulazion
    INI_FILE="/marconi/home/userexternal/dgiaiott/src/wrfchem4units/etc/wrf_chem-simulation.ini.EYJAFJOLL"
#
#   CHECK INITIALIZATION FILE AVAILABILITY THEN LOAD IT	
#
    echo -en "\n\n\tLoking for initialization file for this simulation ..."
    if [[ -e $INI_FILE ]]; then
       echo -en "\tFound.\n\tIt is: $INI_FILE \n\tLoad initialization file ..."
       GOON=0
       source $INI_FILE || GOON=1
       if [[ $GOON -eq 0 ]];then 
          echo -en " successfuly loaded."
       else
          echo -en " ERROR in loading initialization file. I cannot continue.\n\tEXIT\n\n"
          EXIT_STATUS=2; exit $EXIT_STATUS
       fi
    else
       echo -en "... NOT FOUND!!!\n\tIt should be: $INI_FILE\n\tI cannot continue.\n\tEXIT\n\n" 
       EXIT_STATUS=1; exit $EXIT_STATUS
    fi
#
#
#   LOAD NEEDED ENVIRONMENTAL MODULES
#   From initialization file 
  module load $MODULE_TO_LOAD        
  module list
#
#
#
#   SOME DIAGNOSTIC USEFUL FOR DEBUG 
#
#
cat <<EOF
===================================================================================
ENVIRONMENT VARIABLES
       All the environmental variables available from env command
EOF
   env
cat <<EOF
===================================================================================
INPUT ENVIRONMENT VARIABLES
       Upon  startup,  sbatch  will  read and handle the options set in the following 
       environment variables.
EOF
   env | grep "SBATCH" || echo -e "No SBATCH environmental variables found!\n"
cat <<EOF
===================================================================================
OUTPUT ENVIRONMENT VARIABLES
       The Slurm controller will set the following variables in the environment of 
       the batch script.
EOF
   env | grep "MPIRUN_"  || echo -e "No MPIRUN_ environmental variables found!\n"
   env | grep "SLURM"    || echo -e "No SLURM  environmental variables found!\n"
cat <<EOF
===================================================================================
EOF
#   SET GENERAL PARAMETERS AND VARIABLES
#   From initialization file 
#
#
#   EXPORT VARIABLES REQUIRED IN THE ENVIRONMENT
#   From initialization file 
#
  echo -e "\n\tSome parameters are:\n\tHOME_WORK=$HOME_WORK\n\tWRFMODEL_ROOT=$WRFMODEL_ROOT" 
  echo -e "\tPATH=${PATH}"
  echo -e "\tLIBPATH=${LIBPATH}"
  echo -e "\tLD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
#
# Create the directory where the real.exe prep_chem_sources_RADM_WRF_FIM_.exe,
# the convert_emiss.exe and the real.exe will run and the emissions will be generated 
#
  if [[ ! -d ${WRF_RUN_DIR} ]];then mkdir -p ${WRF_RUN_DIR} ; fi
  cd ${WRF_RUN_DIR}
  echo -e "\n\tNow I am in: $(pwd)\n"
#
# +-------------------------------------------------------------------------+
# |                                                                         |
# | SECTION FOR EMISSIONS GENERATION: prep_chem_sources_RADM_WRF_FIM_.exe   |
# |                                                                         |
# +-------------------------------------------------------------------------+
#
# Some cleaning before to start. Emissions are removed
# files *-g1-ab.bin
  for F in $(ls -1 *-g1-ab.bin);do rm -v $F; done
# files *-g1-bb.bin
  for F in $(ls -1 *-g1-bb.bin);do rm -v $F; done
# files *-g1-volc.bin
  for F in $(ls -1 *-g1-volc.bin);do rm -v $F; done
# files *-g1-gocartBG.bin
  for F in $(ls -1 *-g1-gocartBG.bin);do rm -v $F; done
#
# Check whether the executables, the initialization files are available
# otherwise exit
#
  echo -e "\n\t\tSECTION FOR EMISSIONS GENERATION:\n"
  echo -e "\n\tLooking for executable code: prep_chem_sources_RADM_WRF_FIM_.exe"
  if [[ -x ${PREP_CHEM_EXE} ]];then
     echo -e "\tFound executable: ${PREP_CHEM_EXE}"
     ln -svf ${PREP_CHEM_EXE} ./prep_chem_sources_RADM_WRF_FIM_.exe
  else
     echo -e "\tERROR!!! Executable file NOT foud.\n\tIt should be: ${PREP_CHEM_EXE}\n\n"
     EXIT_STATUS=1; exit $EXIT_STATUS
  fi
  echo -e "\n\tLooking for emission databases: Global_emissions_v3"
  if [[ -d ${PREP_CHEM_ROOT_DATA} ]];then
     echo -e "\tFound emission data root dir: ${PREP_CHEM_ROOT_DATA}"
     ln -svf ${PREP_CHEM_ROOT_DATA} ./Global_emissions_v3
  else
     echo -e "\tERROR!!! Eemission databases: Global_emissions_v3 NOT foud.\n\tIt should be: ${PREP_CHEM_ROOT_DATA}\n\n"
     EXIT_STATUS=1; exit $EXIT_STATUS
  fi
  echo -e "\n\tLooking for template input file: prep_chem_sources.inp.tpl"
  if [[ -e ${PREP_CHEM_INP} ]];then
     echo -e "\tFound template file: ${PREP_CHEM_INP}"
  else
     echo -e "\tERROR!!! Template file NOT foud.\n\tIt should be: ${PREP_CHEM_INP}\n\n"
     EXIT_STATUS=1; exit $EXIT_STATUS
  fi
  echo -e "\n\tLooking for ash and SO2 input file: volcano.txt"
  VOLCAN_EMI_FILE="./volcano.txt"
  if [[ -e ${VOLCAN_EMI_FILE} ]];then rm ${VOLCAN_EMI_FILE};fi
  if [[ -e ${PREP_CHEM_VOL} ]];then
     echo -e "\tFound Ash and SO2 file: ${PREP_CHEM_VOL}"
     ln -svf ${PREP_CHEM_VOL} $VOLCAN_EMI_FILE
  else
     echo -e "\tAsh and SO2 file NOT foud.\n\tI am going to use default emissions\n\n"
     VOLCAN_EMI_FILE="NONE"
  fi
  DR="${PREP_CHEM_DTI}" 
  echo -e "\n\tGenerate the prep_chem_sources.inp file from template:\n\t${PREP_CHEM_INP}\n"
  echo -e "\tHere are some details:\n\t\tVolcano = $(grep "volcano_index" ${PREP_CHEM_INP})"
  if [[ -e ${VOLCAN_EMI_FILE} ]];then
     echo -e "\t\tEmissions from file: ${VOLCAN_EMI_FILE}\n"; cat ${VOLCAN_EMI_FILE} | sed s/^/\\t/g
  else
     echo -e "\t\tEmissions from default: see PREP-CHEM-SRC-1.5/src/volc_emissions.f90 code"
  fi
  echo -e "\n\tStart background emissions, if any: $DS\n\tStart volcano emissions: $DR\n" 
  SEDFILE="./$(mktemp -u sedfile.XXXXX)"
  cat << EOF > $SEDFILE
    s/%SYYYY%/${DS:0:4}/g
    s/%SMM%/${DS:4:2}/g
    s/%SDD%/${DS:6:2}/g
    s/%SHH%/${DS:8:2}/g
    s/%VYYYY%/${DR:0:4}/g
    s/%VMM%/${DR:4:2}/g
    s/%VDD%/${DR:6:2}/g
    s/%VHH%/${DR:8:2}/g
    s/%VMIN%/${DR:10:2}/g
    s/%VOLCAN_EMI_FILE%/${VOLCAN_EMI_FILE//\//\\/}/g
EOF
  sed -f $SEDFILE ${PREP_CHEM_INP} > ./prep_chem_sources.inp
  if [[ -e $SEDFILE ]];then rm $SEDFILE ;fi 
#
#
# Run prep_chem_sources_RADM_WRF_FIM_.exe
#
  echo -e "\n\tRunning prep_chem_sources_RADM_WRF_FIM_.exe\n"
  ./prep_chem_sources_RADM_WRF_FIM_.exe > ./prep_chem_sources-$(date -u +%Y%m%d-%H%M%S).log 2>&1
  echo -e "\n\tCompleted\n"
#
#
# Link input emission files as expected by real.exe
#
# files *-g1-ab.bin
  for F in $(ls -1 *-g1-ab.bin);do ln -svf $F emissopt3_d01;done
# files *-g1-bb.bin
  for F in $(ls -1 *-g1-bb.bin);do ln -svf $F emissfire_d01;done
# files *-g1-volc.bin
  for F in $(ls -1 *-g1-volc.bin);do ln -svf $F volc_d01;done
# files *-g1-gocartBG.bin
  for F in $(ls -1 *-g1-gocartBG.bin);do ln -svf $F wrf_gocart_backg;done
#
#
#
# Generate the namelist file to run real.exe  for emissions inputs 
#
# First compute run length in hours
  RUNHH=$((($(date -u -d "${DE:0:8} ${DE:8:2}" +%s) - $(date -u -d "${DS:0:8} ${DS:8:2}" +%s))/3600))
  echo -e "\n\tCustomize namelist for WRF. START DATE is: $DS - END DATE is: $DE (Run hours: $RUNHH).\n\n"
  SEDFILE="./$(mktemp -u sedfile.XXXXX)"
  cat << EOF > $SEDFILE
    s/%SYYYY%/${DS:0:4}/g
    s/%SMM%/${DS:4:2}/g
    s/%SDD%/${DS:6:2}/g
    s/%SHH%/${DS:8:2}/g
    s/%EYYYY%/${DE:0:4}/g
    s/%EMM%/${DE:4:2}/g
    s/%EDD%/${DE:6:2}/g
    s/%EHH%/${DE:8:2}/g
    s/%RUNHH%/${RUNHH}/g
    s/%CHEM_OPT%/${CHEM_OPT}/g
EOF
  sed -f $SEDFILE $WRFCHEM_NAMELIST_TPL > namelist.input
  if [[ -e $SEDFILE ]];then rm $SEDFILE ;fi 
#
#
# Convert emissions in netCDF files suitable for real.exe formats
#
  if [[ -x  $CONV_EMISS_EXE ]];then 
     echo -e "\n\tFiles emissions converter found. it is:\n\t$CONV_EMISS_EXE "
     cp -vf  $CONV_EMISS_EXE ./convert_emiss.exe
  else
     echo -e "\n\tERROR in finding executable file:\n\t$CONV_EMISS_EXE\n\n\tEmission cannot be converted."
     EXIT_STATUS=1; exit $EXIT_STATUS
  fi
  echo -e "\n\n\tRunning emission converter"
  ln -svf $VEGPARM_4_CONV_EMISS_EXE ./VEGPARM.TBL # Change the vegetation parameters table if needed
  ./convert_emiss.exe 
  echo -e "\n\tCompleted\n"
  CONV_EMI_OUT="convert_emiss-$(date -u +%Y%m%d-%H%M%S)"
  cp -v  rsl.error.0000 ${CONV_EMI_OUT}.error 
  cp -v  rsl.out.0000 ${CONV_EMI_OUT}.out
#
# Link the ink del target wrfchemi_00z_d01 al file wrfchemi_d01
#
  ln -svf wrfchemi_d01 wrfchemi_00z_d01
#
# Run real.exe
#
  echo -e "\n\tRunning real.exe\n"
  srun --mpi=pmi2   ./real.exe
  echo -e "\n\tCompleted\n"
#
# Some diagnostics before to leave on putputs
#
  ls -l $WRF_RUN_DIR | sed s/^/\\t/g
#
#   SOME INFORMATION BEFORE TO EXIT 
#
    D1=$(date -u +%s) # Get the time (seconds) at the end             
    echo -e "\n\tEmissions run END: $(date -u +%F' '%T)"
    echo -e "\tTotal $(($D1 - $D0)) seconds  [$((($D1 - $D0)/60)) minutes]"
    echo -e "\n\n\tTo get the summary of resources used in this job:"
    echo -e "\t sacct -p -l -j $SLURM_JOBID \n\n"
    exit $EXIT_STATUS
